package com.example.marketlesson.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.ToStringExclude;
import org.modelmapper.internal.bytebuddy.build.ToStringPlugin;

import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Market {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String marketName;

    @OneToMany(mappedBy = "market")
    @JsonIgnore
    @ToString.Exclude
    @ToStringPlugin.Exclude
    List<Branch> branch;
}
