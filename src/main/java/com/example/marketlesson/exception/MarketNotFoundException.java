package com.example.marketlesson.exception;

public class MarketNotFoundException extends RuntimeException{
    public MarketNotFoundException(String message){
        super(message);
    }
}
