package com.example.marketlesson.response;

public interface BranchInfo {
    Long getId();
    String getBranchName();
    String getMarketName();
    String getAddressName();
}

