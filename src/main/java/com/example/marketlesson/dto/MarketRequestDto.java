package com.example.marketlesson.dto;

import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MarketRequestDto {

    Long marketId;
    @Size(min = 2,max = 20)
    String marketName;
    String branchName;

}