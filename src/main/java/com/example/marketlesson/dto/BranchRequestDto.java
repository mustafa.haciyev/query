package com.example.marketlesson.dto;

import com.example.marketlesson.response.AddressResponse;
import com.example.marketlesson.response.MarketResponse;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchRequestDto {
    Long id;
    String branchName;
    MarketResponse market;
    AddressResponse address;

}