package com.example.marketlesson.service;


import com.example.marketlesson.dto.MarketRequestDto;
import com.example.marketlesson.dto.MarketResponseDto;
import com.example.marketlesson.entity.Market;

import java.util.List;

public interface MarketService {
    Long createMarket(MarketRequestDto marketRequestDto);

    Market getMarketById(Long id);



    List<MarketResponseDto> findByAll();
}
