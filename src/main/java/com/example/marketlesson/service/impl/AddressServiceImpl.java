package com.example.marketlesson.service.impl;

import com.example.marketlesson.config.AppConfiguration;
import com.example.marketlesson.dto.AddressRequestDto;
import com.example.marketlesson.entity.Address;
import com.example.marketlesson.repo.AddressRepository;
import com.example.marketlesson.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final AppConfiguration marketConfiguration;

    @Override
    public List<Address> getAllAddress() {
        return addressRepository.findAll();
    }

    @Override
    public Address getAddressById(Long id) {
        Optional<Address> byId = addressRepository.findById(id);
        byId.orElseThrow(() -> new RuntimeException());
        return byId.get();
//        if (byId.isPresent()) {
//            return byId.get();
//        }
//        throw new RuntimeException();
    }

    @Override
    public Long saveAddress(AddressRequestDto addressRequestDto) {
        Address address = marketConfiguration.getMapper().map(addressRequestDto, Address.class);
        return addressRepository.save(address).getId();
    }
}
