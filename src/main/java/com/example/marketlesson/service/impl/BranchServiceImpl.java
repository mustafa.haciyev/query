package com.example.marketlesson.service.impl;

import com.example.marketlesson.config.AppConfiguration;
import com.example.marketlesson.dto.BranchRequestDto;
import com.example.marketlesson.entity.Address;
import com.example.marketlesson.entity.Branch;
import com.example.marketlesson.entity.Market;
import com.example.marketlesson.repo.BranchRepository;
import com.example.marketlesson.repo.MarketRepository;
import com.example.marketlesson.response.AddressResponse;
import com.example.marketlesson.response.BranchInfo;
import com.example.marketlesson.response.MarketResponse;
import com.example.marketlesson.service.AddressService;
import com.example.marketlesson.service.BranchService;
import com.example.marketlesson.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {
    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private  final MarketService marketService;
    private final AddressService addressService;

    private final AppConfiguration marketConfiguration;
    private final ModelMapper modelMapper;

    @Override
    public List<BranchInfo> getAllBranchesWithInfo() {
        return branchRepository.getAllBranchesWithInfo();
    }
//***************************************************************************************************************
    //Dto Ile baglanan hissesi
//    @Override
//    public List<BranchRequestDto> getAllBranch() {
//        List<Branch> branches = branchRepository.findAll();
//        List<BranchRequestDto> branchDTOs = new ArrayList<>();
//
//
//        for (Branch branch : branches) {
//            BranchRequestDto branchDTO = modelMapper.map(branch, BranchRequestDto.class);
//            branchDTO.setMarket(modelMapper.map(branch.getMarket(), MarketResponse.class));
//            branchDTO.setAddress(modelMapper.map(branch.getAddress(), AddressResponse.class));
//            branchDTOs.add(branchDTO);
//        }
//
//
//        return branchDTOs;
//    }
//***************************************************************************************************************


//    @Override
//    public BranchRequestDto getBranchById(Long id) {
//        Branch branch = branchRepository.findById(id)
//                .orElseThrow(() -> new BranchNotFoundException("Branch not found with id: " + id));
//
//        BranchRequestDto branchRequestDto = marketConfiguration.getMapper().map(branch, BranchRequestDto.class);
//        branchRequestDto.setMarketId(branch.getMarket().getMarketName());
//        branchRequestDto.setAddressId(branch.getAddress().getAddressName());
//
//        return branchRequestDto;
//    }


//    @Override
//    public Long createBranch(BranchRequestDto branchRequestDto) {
//
//        Address address = addressService.getAddressById(branchRequestDto.getAddressId());
//        Market market = marketService.getMarketById(branchRequestDto.getMarketId());
//
//        Branch branch = Branch.builder()
//                .branchName(branchRequestDto.getBranchName())
//                .address(address)
//                .market(market)
//                .build();
//        return branchRepository.save(branch).getId();
//    }

    @Override
    public Long createBranch(BranchRequestDto branchRequestDto) {
        return null;
    }

    @Override
    public BranchRequestDto getBranchById(Long id) {
        return null;
    }


}