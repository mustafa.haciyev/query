package com.example.marketlesson.service.impl;

import com.example.marketlesson.config.AppConfiguration;
import com.example.marketlesson.dto.MarketRequestDto;
import com.example.marketlesson.dto.MarketResponseDto;
import com.example.marketlesson.entity.Market;
import com.example.marketlesson.exception.MarketNotFoundException;
import com.example.marketlesson.repo.MarketRepository;
import com.example.marketlesson.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {

    private final MarketRepository marketRepository;
    private final AppConfiguration marketConfiguration;


    @Override
    public List<MarketResponseDto> findByAll() {
        List<Market> marketList = marketRepository.findAll();
        List<MarketResponseDto> marketResponseDtoList = new ArrayList<>();
        marketList.forEach(s -> {
            MarketResponseDto marketResponseDto = marketConfiguration.getMapper().map(s, MarketResponseDto.class);
            marketResponseDtoList.add(marketResponseDto);
        });
        return marketResponseDtoList;
    }


    @Override
    public Market getMarketById(Long id) {
        return marketRepository.findById(id).get();
    }



    @Override
    public Long createMarket(MarketRequestDto marketRequestDto) {
        Market market = marketConfiguration.getMapper().map(marketRequestDto, Market.class);
        return marketRepository.save(market).getId();
    }




}
