package com.example.marketlesson.service;

import com.example.marketlesson.dto.BranchRequestDto;
import com.example.marketlesson.entity.Branch;
import com.example.marketlesson.response.BranchInfo;

import java.util.List;

public interface BranchService {
    Long createBranch(BranchRequestDto branchRequestDto);



    BranchRequestDto getBranchById(Long id);

    List<BranchInfo> getAllBranchesWithInfo();

//    List<BranchRequestDto> getAllBranch();

//    List<BranchRequestDto> getAllBranchesWithInfo();




}