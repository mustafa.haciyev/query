package com.example.marketlesson.service;

import com.example.marketlesson.dto.AddressRequestDto;
import com.example.marketlesson.entity.Address;

import java.util.List;

public interface AddressService {
    List<Address> getAllAddress();

    Address getAddressById(Long id);

    Long saveAddress(AddressRequestDto addressRequestDto);
}
