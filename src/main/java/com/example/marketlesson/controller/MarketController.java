package com.example.marketlesson.controller;
import com.example.marketlesson.dto.MarketRequestDto;
import com.example.marketlesson.dto.MarketResponseDto;
import com.example.marketlesson.entity.Market;
import com.example.marketlesson.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {

    private final MarketService marketService;

    @GetMapping
    public List<MarketResponseDto> getAllMarkets() {
        return marketService.findByAll();
    }


    @GetMapping("/{id}")
    public Market getMarketById(@PathVariable Long id){
        return marketService.getMarketById(id);
    }

    @PostMapping
    public Long createMarket(@RequestBody @Valid MarketRequestDto marketRequestDto){
        return   marketService.createMarket(marketRequestDto);
    }

}