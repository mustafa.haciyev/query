package com.example.marketlesson.controller;
import com.example.marketlesson.dto.BranchRequestDto;
import com.example.marketlesson.entity.Branch;
import com.example.marketlesson.response.BranchInfo;
import com.example.marketlesson.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {

    private final BranchService branchService;


@GetMapping
public List<BranchInfo> getAllBranchesWithInfo() {
    return branchService.getAllBranchesWithInfo();
}

    @GetMapping("/{id}")
    public ResponseEntity<BranchRequestDto> getPersonById(@PathVariable Long id) {
        BranchRequestDto branchRequestDto = branchService.getBranchById(id);
        return new ResponseEntity<>(branchRequestDto, HttpStatus.OK);
    }


    @PostMapping
    public Long createBranch(@RequestBody BranchRequestDto branchRequestDto) {
        return branchService.createBranch(branchRequestDto);
    }

}