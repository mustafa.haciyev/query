package com.example.marketlesson.repo;

import com.example.marketlesson.dto.BranchRequestDto;
import com.example.marketlesson.entity.Branch;
import com.example.marketlesson.response.BranchInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BranchRepository extends JpaRepository<Branch,Long> {

//   @Query(value = "select b.id, b.branch_name, m.market_name, a.address_name from branch b inner join market m on m.id = b.m_id inner join address a on a.id = b.a_id",nativeQuery = true)
//   Branch getAllBranch();

//    @Query(value = "SELECT b.id, b.branch_name, m.market_name AS marketName, a.address_name AS addressName " +
//            "FROM branch b " +
//            "INNER JOIN market m ON m.id = b.m_id " +
//            "INNER JOIN address a ON b.a_id = a.id", nativeQuery = true)
//    List<BranchRequestDto> getAllBranchesWithInfoNative();

    //***************************************************************************************************************
    @Query(value = "SELECT b.id, b.branch_name as branchName, m.market_name as marketName, a.address_name as addressName " +
            "FROM branch b " +
            "INNER JOIN market m ON m.id = b.m_id " +
            "INNER JOIN address a ON a.id = b.a_id", nativeQuery = true)
    List<BranchInfo> getAllBranchesWithInfoNative();

    @Query("SELECT b.id as id, b.branchName as branchName, m.marketName as marketName, a.addressName as addressName " +
            "FROM Branch b " +
            "INNER JOIN b.market m " +
            "INNER JOIN b.address a")
    List<BranchInfo> getAllBranchesWithInfo();

    //***************************************************************************************************************
}
